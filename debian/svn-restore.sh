#!/bin/sh

DUMP=repo.svndump.gz
DUMPURL=https://storage.googleapis.com/google-code-archive-downloads/v2/code.google.com/naga/

wget -N "$DUMPURL/$DUMP"

rm -rf naga.svn naga.git
svnadmin create naga.svn
zcat "$DUMP" | svnadmin load naga.svn
git svn clone -s --rewrite-root=https://naga.googlecode.com/svn file://$PWD/naga.svn naga.git
